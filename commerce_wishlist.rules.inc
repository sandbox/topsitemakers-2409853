<?php

/**
 * @file
 * Implementation of rules hooks and actions.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_wishlist_rules_event_info() {
  return array(
    'commerce_wishlist_event_product_added_to_wishlist' => array(
      'label' => t('After adding a product to wishlist'),
      'help' => t('Triggers when a product has been added to wishlist.'),
      'group' => t('Commerce Wishlist'),
      'variables' => array(
        'commerce_wishlist_line_item' => array(
          'type'  => 'commerce_line_item',
          'label' => t('Line item'),
        ),
      ),
    ),
    'commerce_wishlist_event_product_added_to_cart' => array(
      'label' => t('After adding a wishlist product to the cart'),
      'help' => t('Triggers when a wishlist product has been added to the shopping cart.'),
      'group' => t('Commerce Wishlist'),
      'variables' => array(
        'commerce_wishlist_line_item_original' => array(
          'type'  => 'commerce_line_item',
          'label' => t('Line item in the wishlist'),
        ),
        'commerce_wishlist_line_item_new' => array(
          'type'  => 'commerce_line_item',
          'label' => t('New line item'),
        ),
      ),
    ),
    'commerce_wishlist_event_product_purchased' => array(
      'label' => t('After purchasing a product from a wishlist'),
      'help' => t('Triggers when a wishlist product is purchased.'),
      'group' => t('Commerce Wishlist'),
      'variables' => array(
        'commerce_wishlist_line_item_purchased' => array(
          'type'  => 'commerce_line_item',
          'label' => t('Line item'),
        ),
        'commerce_wishlist_line_item_in_wishlist' => array(
          'type'  => 'commerce_line_item',
          'label' => t('Line item in the wishlist'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_wishlist_rules_action_info() {
  return array(
    'commerce_wishlist_action_decrease_quantity_in_wishlist' => array(
      'label' => t('Decrease product quantity in wishlist'),
      'group' => t('Commerce Wishlist'),
      'parameter' => array(
        'commerce_wishlist_line_item_purchased' => array(
          'type'  => 'commerce_line_item',
          'label' => t('Purchased line item'),
        ),
      ),
    ),
  );
}

/**
 * Rules action callback to remove a specific product from user's wishlist.
 */
function commerce_wishlist_action_decrease_quantity_in_wishlist($line_item) {
  if (isset($line_item->data['wishlist_line_item_id'])) {
    $wishlist_line_item = commerce_line_item_load($line_item->data['wishlist_line_item_id']);
    // If purchased quantity is lower than the quantity in wishlist, just update
    // the wishlist line item.
    if (round($wishlist_line_item->quantity) > round($line_item->quantity)) {
      // Update.
      $wishlist_line_item->quantity = $wishlist_line_item->quantity - $line_item->quantity;
      commerce_line_item_save($wishlist_line_item);
    }
    else {
      // Delete.
      commerce_line_item_delete($line_item->data['wishlist_line_item_id']);
    }
  }
}
