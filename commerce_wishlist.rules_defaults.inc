<?php

/**
 * @file
 * Default imported rules of this module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_wishlist_default_rules_configuration() {
  $rules = array();

  // Rule triggered when a product has been added to a wishlist.
  $rules['rules_product_is_added_to_the_wishlist'] = entity_import('rules_config', '{"rules_product_is_added_to_the_wishlist":{"LABEL":"Product is added to the wishlist","PLUGIN":"reaction rule","OWNER":"rules","TAGS":["Commerce Wishlist"],"REQUIRES":["rules","commerce_wishlist"],"ON":{"commerce_wishlist_event_product_added_to_wishlist":[]},"DO":[{"drupal_message":{"message":"\u003Cem\u003E[commerce-wishlist-line-item:commerce-product:title]\u003C\/em\u003E has been added to wishlist \u003Ca href=\u0022[commerce-wishlist-line-item:order:commerce-wishlist-url]\u0022\u003E[commerce-wishlist-line-item:order:commerce-wishlist-title]\u003C\/a\u003E."}}]}}');

  // Rule triggered when a wishlist product has been purchased.
  $rules['rules_purchasing_product_from_a_wishlist'] = entity_import('rules_config', '{"rules_purchasing_product_from_a_wishlist":{"LABEL":"Purchasing product from a wishlist","PLUGIN":"reaction rule","OWNER":"rules","TAGS":["Commerce Wishlist"],"REQUIRES":["rules","commerce_wishlist"],"ON":{"commerce_wishlist_event_product_purchased":[]},"DO":[{"mail":{"to":["commerce-wishlist-line-item-in-wishlist:order:owner:mail"],"subject":"A product from your wishlist has been purchased","message":"Hello,\r\n\r\nThe product \u0022\u0022 has been purchased from your wishlist.\r\n\r\nYou can see your wishlist anytime at the following address:\r\n\r\n[site:url]user\/login?destination=[commerce-wishlist-line-item:order:commerce-wishlist-url]\r\n\r\nThank you for being a member of [site:name].\r\n\r\n*****\r\n\r\nThis email has been generated automatically.","language":[""]}},{"commerce_wishlist_action_decrease_quantity_in_wishlist":{"commerce_wishlist_line_item_purchased":["commerce_wishlist_line_item_purchased"]}},{"drupal_message":{"message":"You have successfully purchased a product from wishlist. The user \u003Cem\u003E[commerce-wishlist-line-item-purchased:order:owner:name]\u003C\/em\u003E has been notified by email."}}]}}');

  return $rules;
}
