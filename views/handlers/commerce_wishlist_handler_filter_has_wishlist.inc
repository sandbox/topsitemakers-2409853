<?php

/**
 * @file
 * Filter handler which filters the users with wishlist.
 */

class commerce_wishlist_handler_filter_has_wishlist extends views_handler_filter {

  function admin_summary() { }
  function operator_form(&$form, &$form_state) { }
  function can_expose() {
    return FALSE;
  }

  function query() {
    $this->query->add_where($this->options['group']);
    $this->query->leftJoin('commerce_order', 'o', 'u.uid = o.uid');
    $this->query->add_where($this->options['group']);
  }

}
