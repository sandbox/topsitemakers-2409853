<?php

/**
 * @file
 * Field handler to present a button to remove a line item. It's a dummy
 * handler, most part of the implementation is done via pre and post render
 * hooks.
 */

/**
 * Field handler to present a button to delete a line item.
 */
class commerce_wishlist_handler_field_add_to_cart_from_wishlist extends views_handler_field_entity {

  function construct() {
    parent::construct();
    $this->additional_fields['quantity'] = 'quantity';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['default_quantity']  = array('default' => 1);
    $options['show_quantity']     = array('default' => TRUE);
    $options['wishlist_quantity'] = array('default' => TRUE);
    $options['combine']           = array('default' => TRUE);
    $options['display_path']      = array('default' => FALSE);
    $options['line_item_type']    = array('product' => t('Product'));

    return $options;
  }

  /**
   * Provide the add to cart display options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['show_quantity'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display a textfield quantity widget on the add to cart form.'),
      '#default_value' => $this->options['show_quantity'],
    );

    $form['wishlist_quantity'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use desired quantity'),
      '#description' => t('Use the desired wishlist quantity rather then the default value of 1.'),
      '#default_value' => $this->options['wishlist_quantity'],
      '#dependency' => array('edit-options-show-quantity' => array(1)),
    );

    $form['combine'] = array(
      '#type' => 'checkbox',
      '#title' => t('Attempt to combine like products on the same line item in the cart.'),
      '#description' => t('The line item type, referenced product, and data from fields exposed on the Add to Cart form must all match to combine.'),
      '#default_value' => $this->options['combine'],
    );

    // Add a conditionally visible line item type element.
    $types = commerce_product_line_item_types();

    if (count($types) > 1) {
      $form['line_item_type'] = array(
        '#type' => 'select',
        '#title' => t('Add to Cart line item type'),
        '#options' => array_intersect_key(commerce_line_item_type_get_name(), drupal_map_assoc($types)),
        '#default_value' => !empty($this->options['line_item_type']) ? $this->options['line_item_type'] : 'product',
      );
    }
    else {
      $form['line_item_type'] = array(
        '#type' => 'hidden',
        '#value' => key($types),
      );
    }

    if ($this->view->display[$this->view->current_display]->display_plugin == 'page') {
      $title = t("Link products added to the cart from this page display to the View's path.");
    }
    else {
      $title = t('Link products added to the cart from this display to the current path the customer is viewing where the View is rendered.');
    }

    $form['display_path'] = array(
      '#type' => 'checkbox',
      '#title' => $title,
      '#default_value' => $this->options['display_path'],
    );
  }

  /**
   * Placeholder, required by views.
   */
  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    // Since the default views don't have a path. We need to set this manually.
    $query = drupal_get_query_parameters($_GET, array('q'));
    $form ['#action'] = url(current_path(), array('query' => $query));

    // At this point, the query has already been run, so we can access the
    // results in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {
      // Determine the default value.
      $line_item = $this->get_value($row);
      $quantity = $this->get_value($row, 'quantity');
      $default = $this->options['wishlist_quantity'] ? round($quantity) : 1;

      $form[$this->options['id']][$row_id]['#line_item_id'] = $line_item;
      $form[$this->options['id']][$row_id]['quantity'] = array(
        '#type' => 'textfield',
        '#title' => t('Quantity'),
        '#datatype' => 'integer',
        '#default_value' => $default,
        '#maxlength' => max(4, strlen($quantity)),
        '#size' => 4,
        '#element_validate' => array('element_validate_integer_positive'),
      );
      $form[$this->options['id']][$row_id]['button'] = array(
        '#type' => 'submit',
        '#value' => t('Purchase'),
        '#name' => 'purchase-line-item-' . $row_id,
        '#attributes' => array('class' => array('purchase-line-item')),
        '#line_item_id' => $line_item,
        '#add_to_cart' => TRUE,
        '#row_id' => $row_id,
      );
    }
  }

  /**
   * Handle form submission.
   */
  function views_form_submit($form, &$form_state) {
    if (isset($form_state['clicked_button']['#add_to_cart']) && isset($form_state['clicked_button']['#line_item_id'])) {
      // Get the commerce line item ID.
      $line_item = commerce_line_item_load($form_state['clicked_button']['#line_item_id']);
      $row_id = $form_state['clicked_button']['#row_id'];
      self::add_item_to_cart($form_state, $line_item, $row_id);
    }
  }

  function add_to_cart_from_wishlist($form, &$form_state) {
    // Loop through all rows.
    foreach ($form_state['complete form']['add_to_cart_from_wishlist'] as $row_id => $line_item) {
      if (!is_integer($row_id)) {
        continue;
      }

      // Get the commerce line item.
      $line_item = commerce_line_item_load($line_item['#line_item_id']);
      self::add_item_to_cart($form_state, $line_item, $row_id);
    }
  }

  function add_item_to_cart($form_state, $line_item, $row_id) {
    global $user;

    // Alter the order ID and line item ID values to avoid overwriting.
    $new_line_item = clone($line_item);
    // Alter the quantity for this line item and add only as much as it has
    // been entered in the quantity field.
    $new_line_item->quantity = $form_state['values']['add_to_cart_from_wishlist'][$row_id]['quantity'];
    // Save the original line item ID, so we can pass it to rules after the
    // checkout is completed.
    $new_line_item->data['wishlist_line_item_id'] = $line_item->line_item_id;
    // Invoke rules.
    rules_invoke_all('commerce_wishlist_event_product_added_to_cart', $line_item, $new_line_item);
    // Remove the ID and save this item to the cart.
    unset($new_line_item->line_item_id);
    commerce_cart_product_add($user->uid, $new_line_item);
  }
}

