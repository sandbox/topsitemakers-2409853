<?php

/**
 * @file
 * Field handler to present a button to remove a line item. It's a dummy
 * handler, most part of the implementation is done via pre and post render
 * hooks.
 */

/**
 * Field handler to present a button to delete a line item.
 */
class commerce_wishlist_handler_field_edit_delete extends views_handler_field {

  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    // Since the default views don't have a path. We need to set this manually.
    $query = drupal_get_query_parameters($_GET, array('q'));
    $form ['#action'] = url(current_path(), array('query' => $query));

    // At this point, the query has already been run, so we can access the
    // results in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {
      $line_item_id = $this->get_value($row);

      $form[$this->options['id']][$row_id] = array(
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#name' => 'delete-line-item-' . $row_id,
        '#attributes' => array('class' => array('delete-line-item')),
        '#line_item_id' => $line_item_id,
        // This is necessary to differentiate between the "Remove" and "Add to
        // cart" buttons, as they will both trigger views_form_submit()
        // function.
        '#remove' => TRUE,
      );
    }
  }

  /**
   * Handle form submission.
   */
  function views_form_submit($form, &$form_state) {
    // There is no need for invoking a rule here, because we are using the
    // commerce_line_item_delete() function which will trigger a rule defined in
    // commerce_line_item module. User can then do a data comparison to check if
    // the parent order is in Wishlist state. Same effect.
    //
    // If this is a "Remove" button for a line item, delete this line item.
    if (isset($form_state['clicked_button']['#remove']) && isset($form_state['clicked_button']['#line_item_id'])) {
      commerce_line_item_delete($form_state['clicked_button']['#line_item_id']);
    }
    // Note: no need to do anything else here. The quantity box will update the
    // product quantity in Wishlist order automatically.
  }

}
