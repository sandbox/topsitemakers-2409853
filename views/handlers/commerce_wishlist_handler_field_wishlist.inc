<?php

/**
 * @file
 * Filter handler which filters the users with wishlist.
 */

class commerce_wishlist_handler_field_wishlist extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Don't add the additional fields to groupby
    if (!empty($this->options['link_to_wishlist'])) {
      $this->additional_fields['order_id'] = array(
        'table' => 'commerce_order',
        'field' => 'order_id',
      );
      $this->additional_fields['uid'] = array(
        'table' => 'commerce_order',
        'field' => 'uid',
      );
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_wishlist'] = array(
      'default' => isset($this->definition['link_to_wishlist default']) ? $this->definition['link_to_wishlist default'] : TRUE,
      'bool' => TRUE,
    );
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_wishlist'] = array(
      '#title' => t('Link this field to the wishlist.'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_wishlist']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_wishlist']) && !empty($this->additional_fields['order_id']) && !empty($this->additional_fields['uid'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = commerce_wishlist_construct_uri('view', $this->get_value($values, 'uid'), $this->get_value($values, 'order_id'));
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }
}
