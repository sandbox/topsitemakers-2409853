<?php

/**
 * All Views related implementations.
 */

/**
 * Implements hook_views_data_alter()
 */
function commerce_wishlist_views_data_alter(&$data) {
  // Adds a textfield to edit line item quantity on the view.
  $data['commerce_line_item']['edit_wishlist_quantity'] = array(
    'real field' => 'line_item_id',
    'field' => array(
      'title' => t('Wishlist: quantity text field'),
      'help' => t('Adds a text field to edit the line item quantity in the wishlist.'),
      'handler' => 'commerce_wishlist_handler_field_edit_quantity',
    ),
  );
  // Expose the remove from wishlist button.
  $data['commerce_line_item']['remove_from_wishlist'] = array(
    'title' => t('Wishlist: remove button'),
    'help' => t('Generates a button for removing the product from wishlist.'),
    'real field' => 'line_item_id',
    'field' => array(
      'title' => t('Wishlist: remove button'),
      'help' => t('Adds a button to delete a line item.'),
      'handler' => 'commerce_wishlist_handler_field_edit_delete',
    ),
  );
  // Expose the add to cart button.
  // $data['commerce_product']['add_to_cart_form']['moved to'] = array('views_entity_commerce_product', 'add_to_cart_from_wishlist');
  $data['commerce_line_item']['add_to_cart_from_wishlist'] = array(
    'title' => t('Wishlist: add to cart button'),
    'help' => t('Generates a button for adding the product from wishlist to the shopping cart.'),
    'real field' => 'line_item_id',
    'field' => array(
      'title' => t('Wishlist: add to cart button'),
      'help' => t('Adds an add to cart button to a wishlist line item.'),
      'handler' => 'commerce_wishlist_handler_field_add_to_cart_from_wishlist',
    ),
  );
  // Add a custom filter for checking if a user has a wishlist.
  $data['user']['wishlist'] = array(
    'title' => t('Has wishlist'),
    'help' => t('Filter only users who have an active wishlist order.'),
    // Information for searching terms using the full search syntax
    'filter' => array(
      'handler' => 'commerce_wishlist_handler_filter_has_wishlist',
      'no group by' => TRUE,
    ),
  );
}
