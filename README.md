
*******
GENERAL
*******

This module provides Wishlist functionality. With it, you can allow your users
to create private or public wishlists, and save products to it.

It is a complete rewrite of `v1` branch of `Commerce Wishlist` module.

## Sharing the wishlists

Current version of the module does not support emailing links to the wishlist.

Emailing the wishlist might be added as a sub-module at a later stage.
For more information, see [this][0] issue.

The easiest way to achieve this functionality right now, is to install a share
service module, such as [AddThis][1], which will then allow users to share the
page they are on to any social network or send it directly via email.

## Credits

To be added once the project reaches mature state.

***************
DEVELOPER NOTES
***************

Here's how the module works in a nutshell:

- The module provides a new order status: `Wishlist`. This means that user
  wishlists are actually orders with `Wishlist` status. This gives us complete
  support of any line item customizations (e.g. custom pricing attributes, etc.)
- It adds a new button to the `Add to cart` form called `Add to wishlist`. The
  text of this button can be changed via administrative interface.
- Submit handler of the `Add to wishlist` form performs pretty much the same
  actions as the default Commerce `Add to cart` handler, except one difference:
  the order is saved with the new `Wishlist` status to user account.
- All additional actions are handled by Commerce, Rules and Views. The module
  provides a Views access plugin that checks if contents of an order should be
  visible to other users. This means that depending on the setup (i.e. if the
  acting user has necessary permissions) user will (or will not) be granted
  access to a wishlist.

Everything is made to be as customizable as possible, with many ways to override
default functionality. See the following hooks to learn what can be overridden
through custom modules or themes:

- `hook_theme()` implementation: `commerce_wishlist_theme()`.
- Administrative callbacks declared in `commerce_wishlist.admin.inc` file.
- See helper functions provided by this module in the
  `commerce_wishlist.helpers.inc` file.
- Read comments and

*************************
CONTRIBUTING AND FEEDBACK
*************************

Feel free to contribute or provide feedback in [module's issue queue][2].

[0] https://www.drupal.org/node/2570957
[1] https://www.drupal.org/project/addthis
[2] https://www.drupal.org/project/issues/2409853?categories=1
