<?php

/**
 * @file
 * Helper functions used in the module.
 */

/**
 * Loads and caches the available wishlists for all subsequent forms.
 *
 * @param int $uid The user id to fetch the wishlists for.
 * @return array Wishlist titles keyed by commerce_order order_id.
 */
function _commerce_wishlist_get_user_wishlist_titles($uid) {
  $options = &drupal_static(__FUNCTION__);

  if (!isset($options[$uid])) {
    $options[$uid] = array();

    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'commerce_order')
      ->entityCondition('bundle', 'commerce_order')
      ->propertyCondition('uid', $uid)
      ->propertyCondition('state', 'wishlist');

    $result = $query->execute();

    if (isset($result['commerce_order'])) {
      $wishlists = array_keys($result['commerce_order']);
      $wishlists = entity_load('commerce_order', $wishlists);

      foreach ($wishlists as $order_id => $wishlist) {
        $options[$uid][$order_id] = commerce_wishlist_get_wishlist_title($wishlist);
      }
    }
  }

  return $options[$uid];
}

/**
 * Helper function for retrieving the wishlist title from a commerce order.
 *
 * @return string Rendered wishlist title.
 */
function commerce_wishlist_get_wishlist_title($commerce_order) {
  $title = field_get_items('commerce_order', $commerce_order, 'commerce_wishlist_title');
  $output = field_view_value('commerce_order', $commerce_order, 'commerce_wishlist_title', $title[0]);

  return render($output);
}

/**
 * Helper function for rendering 'commerce_wishlist' Views as page content.
 *
 * @return array Renderable array.
 */
function commerce_wishlist_embed_view_commerce_wishlist($account, $order) {
  global $user;
  // Set the correct display based on the user who is viewing the order.
  $view = 'commerce_wishlist_view_wishlist';
  if ($user->uid == $order->uid) {
    $view = 'commerce_wishlist_administer_wishlist';
  }

  list($account) = entity_extract_ids('user', $account);
  list($order) = entity_extract_ids('commerce_order', $order);
  return views_embed_view($view, 'default', $account, $order);
}

/**
 * Helper function for rendering 'wishlist overview' Views as page content.
 *
 * @return array Renderable array.
 */
function commerce_wishlist_embed_view_commerce_wishlist_overview($account) {
  list($account) = entity_extract_ids('user', $account);
  return views_embed_view('commerce_wishlist_overview', 'default', $account);
}

/**
 * Helper function for constructing for building commerce wishlist uri's.
 *
 * @param string $action One of 'view', 'edit' or 'delete'.
 * @param int $uid ID of the owner of the wishlist.
 * @param int $order_id ID of the corresponding commerce order.
 *
 * @return string Constructed URI for the requested action.
 */
function commerce_wishlist_construct_uri($action, $uid, $order_id = NULL) {
  switch ($action) {
    case 'overview':
      return 'user/' . $uid . '/wishlist';

    case 'view':
      return 'user/' . $uid . '/wishlist/' . $order_id;

    case 'edit':
      return 'user/' . $uid . '/wishlist/' . $order_id . '/edit';

    case 'delete':
      return 'user/' . $uid . '/wishlist/' . $order_id . '/delete';
  }
}

/**
 * Entity metadata callback: returns the current user's shopping cart order.
 *
 * @see commerce_wishlist_entity_property_info_alter()
 */
function commerce_wishlist_get_properties($data, array $options, $name) {
  switch ($name) {
    case 'commerce_wishlist_url':
      return url(commerce_wishlist_construct_uri('view', $data->uid, $data->order_id), $options);

    case 'commerce_wishlist_edit_url':
      return url(commerce_wishlist_construct_uri('edit', $data->uid, $data->order_id), $options);
  }
}
