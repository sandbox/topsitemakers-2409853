<?php

/**
 * @file
 * Contains all commerce hook implementations.
 */

/**
 * Implements hook_commerce_order_status_info().
 */
function commerce_wishlist_commerce_order_status_info() {
  $order_statuses = array();

  $order_statuses['wishlist'] = array(
    'name'  => 'wishlist',
    'title' => t('Wishlist'),
    'state' => 'wishlist',
    // Has to stay FALSE, because Commerce will then prevent the products from
    // being purchased when the user adds something else to the shopping cart.
    'cart'  => FALSE,
  );

  return $order_statuses;
}

/**
 * Implements hook_commerce_order_state_info().
 */
function commerce_wishlist_commerce_order_state_info() {
  $order_states = array();

  $order_states['wishlist'] = array(
    'name' => 'wishlist',
    'title' => t('Wishlist'),
    'description' => t('Orders in this state contain wishlist products.'),
    'weight' => -5,
    'default_status' => 'wishlist',
  );

  return $order_states;
}

/**
 * Implements hook_commerce_checkout_complete().
 */
function commerce_wishlist_commerce_checkout_complete($order) {
  // Loop through the line items and see if they are referencing an existing
  // wishlist line item.
  foreach ($order->commerce_line_items[LANGUAGE_NONE] as $order_line_item) {
    $line_item = commerce_line_item_load($order_line_item['line_item_id']);
    if (isset($line_item->data['wishlist_line_item_id'])) {
      // This way we make sure that the original line item still exists.
      $wishlist_line_item = commerce_line_item_load($line_item->data['wishlist_line_item_id']);
      if ($wishlist_line_item) {
        // The wishlist line item has been found. Invoke the appropriate rule.
        rules_invoke_all('commerce_wishlist_event_product_purchased', $line_item, $wishlist_line_item);
      }
    }
  }
}

/**
 * Implements hook_commerce_entity_access_condition_ENTITY_TYPE_alter().
 */
function commerce_wishlist_commerce_entity_access_condition_commerce_order_alter(&$conditions, $context) {
  // Stop right here if the current user does not have access to wishlists.
  if(!user_access('view own wishlists') || !user_access('view all wishlists')) {
    return;
  }

  // This works only for orders of wishlist type.
  $and = db_and();
  $and->condition($context['base_table'] . '.status', 'wishlist');

  // If the user can view his own wishlists, make sure we filter on it.
  if(user_access('view own wishlists')) {
    $and->condition($context['base_table'] . '.uid', $context['account']->uid);
  }

  $or = db_or();
  $or->condition($and);

  $conditions->condition($or);
}
