<?php

/**
 * @file
 * Administrative pages of the module.
 */

/**
 * Main configuration form.
 */
function commerce_wishlist_admin_form() {

  // General wishlist settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
  );
  $form['general']['commerce_wishlist_var_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Product types'),
    '#description' => t('Select on which product types should the "Add to wishlist" button be displayed. If left empty, the button will be displayed on all product types.'),
    '#options' => commerce_product_type_options_list(),
    '#default_value' => variable_get('commerce_wishlist_var_product_types', array()),
  );
  $form['general']['commerce_wishlist_var_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button text'),
    '#description' => t('Enter the text which should be displayed on the button.'),
    '#default_value' => variable_get('commerce_wishlist_var_button_text', t('Add to wishlist')),
    '#required' => TRUE,
  );
  $form['general']['commerce_wishlist_var_select_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for selecting an existing wishlist'),
    '#description' => t('Enter the label text for the dropdown of user\'s wishlists.'),
    '#default_value' => variable_get('commerce_wishlist_var_select_label', t('Select a wishlist:')),
    '#required' => TRUE,
  );
  $form['general']['commerce_wishlist_var_create_new_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for creating a new wishlist'),
    '#description' => t('This text is displayed as a last option of "@select_label" dropdown.', array(
      '@select_label' => variable_get('commerce_wishlist_var_select_label', t('Select a wishlist:')),
    )),
    '#default_value' => variable_get('commerce_wishlist_var_create_new_label', t('- Create new -')),
    '#required' => TRUE,
  );

  // Settings for users without permissions.
  $form['no_perms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for users without permissions'),
    '#collapsible' => TRUE,
  );
  $form['no_perms']['commerce_wishlist_var_no_perms_mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#description' => t('Choose the desired behavior when the user does not have enough permissions to add a product to the wishlist.'),
    '#default_value' => variable_get('commerce_wishlist_var_no_perms_mode', 'theme'),
    '#options' => array(
      'theme' => t('Show a themable message'),
      'hide' => t('Hide the message'),
    ),
  );

  return system_settings_form($form);
}

/**
 * Main commerce_wishlist form.
 */
function commerce_wishlist_form($form, &$form_state, $commerce_order = NULL) {
  global $user;
  $uid = $user->uid;

  $order = $commerce_order;
  if (empty($commerce_order)) {
    // Prepare empty order if none given.
    $order = commerce_order_new($uid, 'wishlist');
  }

  // Fetch a list of all field instances on commerce orders.
  $fields = _field_invoke_get_instances('commerce_order', 'commerce_order', array('deleted' => FALSE));

  // Attach all field instances with 'commerce_wishlist_show' enabled.
  foreach ($fields as $field => $details) {
    if ($details['settings']['commerce_wishlist_show']) {
      field_attach_form('commerce_order', $order, $form, $form_state, NULL, array('field_name' => $field));
    }
  }

  // Store the entity for later validation and submission.
  $form_state['#commerce_wishlist_order'] = $order;

  // Add the form actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );

  if (!empty($commerce_order->order_id)) {
    $form ['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('commerce_wishlist_form_deletebutton_submit'),
    );
  }

  return $form;
}

/**
 * Validation for the commerce_wishlist form.
 */
function commerce_wishlist_form_validate($form, &$form_state) {
  field_attach_form_validate('commerce_order', $form_state['#commerce_wishlist_order'], $form, $form_state);
}

/**
 * Submission of the commerce_wishlist form.
 */
function commerce_wishlist_form_submit($form, &$form_state) {
  $entity = $form_state['#commerce_wishlist_order'];

  if (commerce_order_save($entity)) {
    field_attach_submit('commerce_order', $entity, $form, $form_state);
    $form_state['redirect'] = commerce_wishlist_construct_uri('view', $entity->uid, $entity->order_id);
    drupal_set_message(t('Wishlist saved successfully.'));
  }
}

/**
 * Handles the delete button on the commerce_wishlist form.
 */
function commerce_wishlist_form_deletebutton_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $commerce_order = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array(commerce_wishlist_construct_uri('delete', $commerce_order->uid, $commerce_order->order_id), array('query' => $destination));
}

/**
 * Delete confirmation form for wishlists.
 */
function commerce_wishlist_form_delete($form, &$form_state, $commerce_order) {
  $form_state['#commerce_order'] = $commerce_order;
  $title = commerce_wishlist_get_wishlist_title($commerce_order);
  $message = t('Are you sure you want to remove wishlist %title?', array(
    '%title' => $title
  ));

  return confirm_form($form, $message, '<front>', NULL, t('Delete'));
}

/**
 * Submit handler for wishlist deletion.
 * @see commerce_wishlist_wishlist_delete_form().
 */
function commerce_wishlist_form_delete_submit($form, &$form_state) {
  $entity = $form_state['#commerce_order'];
  list($id) = entity_extract_ids('commerce_order', $entity);

  // Delete the commerce_order. Wishlist table will stay in sync through
  // hook_entity_delete().
  if (commerce_order_delete($id)) {
    $form_state['redirect'] = commerce_wishlist_construct_uri('overview', $entity->uid);
    drupal_set_message(t('Wishlist has been deleted successfully.'));
  }
}
