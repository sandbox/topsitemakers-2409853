<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 2-10-2015
 * Time: 00:39
 */

/**
 * Implements hook_menu().
 */
function commerce_wishlist_email_menu() {
  // Add E-mail tab to the wishlist overview
  $items['user/%user/wishlist/%commerce_order/email'] = array(
    'title' => 'Email wishlist',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_wishlist_email_email_form', 1, 3),
    'access callback' => 'commerce_wishlist_wishlist_access',
    'access arguments' => array('view', 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10
  );

  return $items;
}

/**
 * Displays the e-mail form().
 */
function commerce_wishlist_email_email_form($form, &$form_state, $account, $commerce_order) {
  global $language;

  // Check if flood control has been activated for sending e-mails.
  $limit = variable_get('commerce_wishlist_email_threshold_limit', 5);
  $window = variable_get('commerce_wishlist_email_threshold_window', 3600);
  if (!flood_is_allowed('commerce_wishlist_email', $limit, $window) && !user_access('administer wishlist configuration')) {
    drupal_set_message(t("You cannot send more than %limit messages in @interval. Try again later.", array('%limit' => $limit, '@interval' => format_interval($window))), 'error');
    drupal_access_denied();
    drupal_exit();
  }

  $form_state['#account'] = $account;
  $form_state['#commerce_order'] = $commerce_order;

  $variables = array(
    'user' => $account,
    'commerce-order' => $commerce_order,
  );

  $form['method'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#options' => array(
      'wishlist' => t('Send the wishlist contents'),
//      'link' => t('Link to the wishlist'),
    ),
    '#description' => t('Select how the wishlist should be sent.'),
    '#required' => TRUE,
    '#default_value' => 'wishlist',
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient name'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient e-mail address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['body'] = array(
    '#title' => t('Body'),
    '#type' => 'textarea',
    '#default_value' => _commerce_wishlist_email_mail_text('commerce_wishlist_email_body', $language, $variables, TRUE),
  );

  $form['tokens'] = array(
    '#theme' => 'token_tree_link',
    // To get these options to work, use the patch from https://www.drupal.org/node/2289203 if you have Token 7.x-1.5 or earlier.
    '#token_types' => array('commerce-order', 'user'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
    '#global_types' => TRUE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
    '#click_insert' => TRUE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
  );

  $form['preview'] = array(
    '#title' => t('Body'),
    '#type' => 'item',
    '#markup' => views_embed_view('commerce_wishlist_view_wishlist', 'default', $account->uid, $commerce_order->order_id),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Form validation
 */
function commerce_wishlist_email_email_form_validate($form, $form_state) {
  // Check for validate email
  $mail = $form_state['values']['address'];
  if (!valid_email_address($mail)) {
    form_set_error('address', t('The email address appears to be invalid.'));
  }
}

/**
 * Form submit
 */
function commerce_wishlist_email_email_form_submit($form, &$form_state) {
  global $user, $language;

  $to = $form_state['values']['address'];

  $params = array(
    'body' => $form_state['values']['body'],
    'preview' => $form_state['complete form']['preview']['#markup'],
    'account' => $user,
    'commerce_order' => $form_state['#commerce_order'],
  );

  if (drupal_mail('commerce_wishlist_email', 'commerce_wishlist_email', $to, $language, $params)) {
    flood_register_event('commerce_wishlist_email', variable_get('commerce_wishlist_email_threshold_window', 3600));
    drupal_set_message(t('Your message has been sent.'));
  }
  else {
    drupal_set_message(t('An error occured while sending the email'), 'error');
  }
}

/**
 * Implements hook_mail().
 */
function commerce_wishlist_email_mail($key, &$message, $params) {
  $language = $message['language'];

  $variables = array(
    'user' => $params['account'],
    'commerce-order' => $params['commerce_order'],
  );

  $message['subject'] .= _commerce_wishlist_email_mail_text($key . '_subject', $language, $variables);
  $message['body'][] = token_replace($params['body'], $variables, array('language' => $language, 'sanitize' => FALSE, 'clear' => TRUE));
  $message['body'][] = $params['preview'];
}

/**
 * Helper function for formatting wishlist mails.
 *
 * @param $key
 * @param null $language
 * @param array $variables
 * @param bool|TRUE $replace
 *
 * @return mixed|null|string
 */
function _commerce_wishlist_email_mail_text($key, $language = NULL, $variables = array(), $replace = TRUE) {
  $langcode = isset($language) ? $language->language : NULL;

  if ($admin_setting = variable_get('commerce_wishlist_email_' . $key, FALSE)) {
    // An admin setting overrides the default string.
    $text = $admin_setting;
  }
  else {
    // No override, return default string.
    switch ($key) {
      case 'commerce_wishlist_email_subject':
        $text = t('Wishlist of [user:name] at [site:name]', array(), array('langcode' => $langcode));
        break;
      case 'commerce_wishlist_email_body':
        $text = t("Dear receipient, Below you will find the [commerce-order:status-title] of [user:name] at [site:name]. You can also view [commerce-order:view-url]

[views:embed:commerce_wishlist_view_wishlist:default:[user:uid]:[commerce-order:order-id]]

Regards,
[site:name]",
          array('@url' => url('foo', array('absolute' => TRUE))),
          array('langcode' => $langcode));
        break;
    }
  }

  if ($replace) {
    // We do not sanitize the token replacement, since the output of this
    // replacement is intended for an e-mail message, not a web browser.
    return token_replace($text, $variables, array('language' => $language, 'sanitize' => FALSE, 'clear' => TRUE));
  }

  return $text;
}
