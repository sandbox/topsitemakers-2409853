
/**
 * @file
 * JavaScript for updating the wishlist block.
 */

(function($){
  Drupal.behaviors.UpdateWishlists = {
    attach: function(context) {

      function updateSelectElements(value) {
        $('form.commerce-add-to-cart')
          .find('.commerce-wishlist-select')
          .each(function() {
            $(this).val(value);
          });
      }

      $('form#commerce-wishlist-block-select-form')
        .find('#edit-commerce-wishlist-block-select')
        .change(function(e) {
          var value = $(this).val();
          updateSelectElements(value);
        });
    }
  }
})(jQuery);
