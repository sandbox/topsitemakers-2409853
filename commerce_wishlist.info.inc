<?php

/**
 * @file
 * Provides metadata for the wishlist order.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function commerce_wishlist_entity_property_info_alter(&$info) {
  // Add the current user's shopping cart to the site information.
  $info['commerce_order']['properties']['commerce_wishlist_url'] = array(
    'label' => t("Wishlist URL"),
    'description' => t("The URL of the wishlist."),
    'getter callback' => 'commerce_wishlist_get_properties',
    'type' => 'uri',
    'computed' => TRUE,
    // Make the field available as views field
    'entity views field' => TRUE,
  );

  // Add the current user's shopping cart to the site information.
  $info['commerce_order']['properties']['commerce_wishlist_edit_url'] = array(
    'label' => t("Wishlist edit URL"),
    'description' => t("The URL of the wishlist's edit page."),
    'getter callback' => 'commerce_wishlist_get_properties',
    'type' => 'uri',
    'computed' => TRUE,
    // Make the field available as views field
    'entity views field' => TRUE,
  );
}
